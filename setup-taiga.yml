---
- hosts: all
  become: true
  gather_facts: yes

  tasks:
  - name: install semanage prereqs and enable EPEL & SCL
    yum:
      name: 
        - libsemanage-python
        - policycoreutils-python
        - epel-release
        - centos-release-scl
      state: installed 

  - name: configure selinux to allow nginx into home dirs 
    seboolean:
      name: httpd_read_user_content
      state: yes
      persistent: yes

  - name: allow webserver to listen on tcp port 8000
    seport:
      ports: 8000
      proto: tcp
      setype: http_port_t
      state: present

  - name: allow webserver to listen on tcp port 8001
    seport:
      ports: 8001
      proto: tcp
      setype: http_port_t
      state: present

  - name: open firewall port 80
    firewalld:
      service: http
      immediate: yes
      permanent: yes
      state: enabled

  - name: open firewall port 8000
    firewalld:
      port: 8000/tcp
      immediate: yes
      permanent: yes
      state: enabled

  - name: get some packages installed
    yum:
      name: 
        - gcc
        - autoconf
        - flex
        - bison
        - libjpeg-turbo-devel
        - freetype-devel
        - zlib-devel
        - zeromq3-devel
        - gdbm-devel
        - ncurses-devel
        - automake
        - libtool
        - libffi-devel
        - curl
        - git
        - tmux
        - libxml2-devel
        - libxslt-devel
        - wget
        - openssl-devel
        - gcc-c++
        - rh-postgresql95
        - rh-postgresql95-postgresql-contrib.x86_64
        - rh-postgresql95-postgresql-devel.x86_64
        - rh-python35
        - nginx
      state: present

  - name: init the postgres db
    command: scl enable rh-postgresql95 "postgresql-setup --initdb --unit rh-postgresql95-postgresql"
    args:
      creates: /var/opt/rh/rh-postgresql95/lib/pgsql/data/pg_clog
    tags: initdb

  - name: start and enable postgres
    systemd:
      name: rh-postgresql95-postgresql
      state: started
      enabled: yes

  - name: create a postgres user for taiga
    command: scl enable rh-postgresql95 'su postgres -c "createuser taiga"'
    ignore_errors: True
    tags: scl

  - name: create the taiga dabase in postres
    command: scl enable rh-postgresql95 'su postgres -c "createdb taiga -O taiga"'
    ignore_errors: True
    tags: scl

  - name: create the taiga useraccount
    user:
      name: taiga

  - name: check is backend is already checked out
    stat:
      path: /home/taiga/taiga-back
    register: backend_directory

  - name: checkout taiga backend from git
    git:
      repo: https://github.com/taigaio/taiga-back.git
      dest: /home/taiga/taiga-back
      version: stable
    when: backend_directory.stat.exists == False

  - name: upgrade pip
    command: scl enable rh-postgresql95 rh-python35 "pip3.5 install --upgrade pip"

  - name: pull down the pip requirements
    command: scl enable rh-postgresql95 rh-python35 "pip3.5 install -r requirements.txt" 
    args:
      chdir: /home/taiga/taiga-back

  - name: ensure ownership is correct
    command: chown -R taiga:taiga /home/taiga/taiga-back
  
  - name: taiga setup step1
    command: scl enable rh-postgresql95 rh-python35 'su taiga -c "python3.5 manage.py migrate --noinput"'
    args:
      chdir: /home/taiga/taiga-back
      creates: /home/taiga/taiga-back/taiga/__pycache__/routers.cpython-35.pyc
    tags: step1

  - name: taiga setup step2
    command: scl enable rh-postgresql95 rh-python35 'su taiga -c "python3.5 manage.py loaddata initial_user"' 
    args:
      chdir: /home/taiga/taiga-back
      creates: /var/opt/rh/rh-postgresql95/lib/pgsql/data/global/pg_control 
    tags: step2

  - name: taiga setup step3
    command: scl enable rh-postgresql95 rh-python35 'su taiga -c "python3.5 manage.py loaddata initial_project_templates"'
    args:
      chdir: /home/taiga/taiga-back
      #creates: /var/opt/rh/rh-postgresql95/lib/pgsql/data/global/pg_internal.init
      #TODO add idempotency test that actally works 
    tags: step3

  - name: taiga setup step4
    command: scl enable rh-postgresql95 rh-python35 'su taiga -c "python3.5 manage.py compilemessages"'
    args:
      chdir: /home/taiga/taiga-back
      creates: /home/taiga/taiga-back/taiga/locale/en/LC_MESSAGES/django.mo
    tags: step4

  - name: taiga setup step5
    command: scl enable rh-postgresql95 rh-python35 'su taiga -c "python3.5 manage.py collectstatic --noinput"'
    args:
      chdir: /home/taiga/taiga-back
      creates: /home/taiga/taiga-back/static/img/user-noimage.png
    tags: step5

  - name: copy the local.py file over
    template:
      src: templates/local.py.j2
      dest: /home/taiga/taiga-back/settings/local.py
    tags: local.py

  - name: check is frontend is already checked out
    stat:
      path: /home/taiga/taiga-front-dist
    register: frontend_directory

  - name: checkout taiga frontend from git
    git:
      repo: https://github.com/taigaio/taiga-front-dist.git
      dest: /home/taiga/taiga-front-dist
      version: stable
    when: frontend_directory.stat.exists == False

  - name: copy example config file
    copy:
      src: /home/taiga/taiga-front-dist/dist/conf.example.json
      dest: /home/taiga/taiga-front-dist/dist/conf.json
      remote_src: yes
 
  - name: edit the config file 
    replace:
      path: /home/taiga/taiga-front-dist/dist/conf.json
      regexp: 'http://localhost:8000'
      replace: ''

  - name: copy basic nginx config over
    copy:
      src: files/nginx.conf 
      dest: /etc/nginx/nginx.conf
    tags: nginx

  - name: copy nginx taiga conf
    copy:
      src: files/taiga.conf
      dest: /etc/nginx/conf.d/taiga.conf
    tags: nginx

  - name: ensure nginx is running
    systemd:
      name: nginx
      state: restarted
      enabled: yes
    tags: nginx

  - name: setup taiga sysemd service file
    copy:
      src: files/taiga.service
      dest: /etc/systemd/system/taiga.service

  - name: ensure ownership is correct
    command: chown -R taiga:taiga /home/taiga/

  - name: ensure mode is correct
    command: chmod o+x /home/taiga/

  - name: systmed daemon reload
    systemd:
      state: restarted
      daemon_reload: yes
      name: taiga
      enabled: yes
    tags: nginx

  - name: upgrade all packages
    yum:
      name: '*'
      state: latest
